/*

  Kombine

  Copyright © 2011-3, 2017-9 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "munipack.h"
#include <wx/wx.h>


bool Munipack::kombine(MuniProcess *action, MuniCmdLineParser& cmd)
{
  double x,y;
  long w,h;
  wxString a;

  CommonOutputSingle(action,cmd);
  CommonOptionsBitpix(action,cmd);

  if( cmd.Found("p",&a) || cmd.Found("projection",&a) )
    action->Write("PROJECTION = '" + a.Upper() + "'");

  if( cmd.Found("width",&w) && cmd.Found("height",&h) )
    action->Write("NAXES = %ld %ld",w,h);

  if( cmd.Found("xcen",&x) && cmd.Found("ycen",&y) )
    action->Write("CRPIX = %20.15f %20.15f",x,y);

  if( cmd.Found("rcen",&x) && cmd.Found("dcen",&y) )
    action->Write("CRVAL = %20.15f %20.15f",x,y);

  if( cmd.Found("pm-ra",&x) && cmd.Found("pm-dec",&y) )
    action->Write("CRMOV = %20.15f %20.15f",x,y);

  if( cmd.Found("pm-jdref",&x) )
    action->Write("JDREF = %20.15f",x);

  if( cmd.Found("scale",&x) )
    action->Write("SCALE = %25.15e",x);

  if( cmd.Found("angle",&x) )
    action->Write("ANGLE = %20.15f",x);

  if( cmd.Found("reflex",&a) ) {
    a.MakeUpper();
    wxString reflex = a.Find("Y") != wxNOT_FOUND ? "T" : "F";
    action->Write("REFLEX = " + reflex);
  }

  if( cmd.Found("i",&a) || cmd.Found("interpol",&a) )
    action->Write("INTERPOL = '" + a.Upper() + "'");

  if( cmd.Found("arith") )
    action->Write("ROBUST = F");

  if( cmd.Found("disable-back") )
    action->Write("BACKGROUND = F");

  WriteFiles(action,cmd.GetFiles());

  return true;
}

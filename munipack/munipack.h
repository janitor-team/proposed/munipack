/*

  Munipack -- the command line interface

  Copyright © 2009-2020 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "../config.h"
#include "mprocess.h"
#include <wx/wx.h>
#include <wx/app.h>
#include <wx/cmdline.h>
#include <wx/process.h>
#include <wx/event.h>


class MuniCmdLineParser: public wxCmdLineParser
{
public:
  MuniCmdLineParser(const wxString&,int,wchar_t **);
  virtual ~MuniCmdLineParser();

  void Usage() const;
  int Parse(bool giveUsage=true);
  size_t GetFilesCount() const { return files.size(); }
  wxString GetFile(size_t n) const;
  wxArrayString GetFiles(size_t n) const;
  wxArrayString GetFiles() const { return files; }

private:

  int argc;
  wchar_t **argv;
  wxArrayString files;
  bool InputByFile(const wxString&);

};


class Munipack: public wxAppConsole
{
public:

  bool OnInit();
  int OnRun();
  void OnFinish(wxProcessEvent&);

private:

  void CommonOptionsBitpix(MuniProcess *, const MuniCmdLineParser&);
  void CommonOptionsPhCorr(MuniProcess *, const MuniCmdLineParser&);
  void CommonOptionsEnableOverwrite(MuniProcess *, const MuniCmdLineParser&);
  void CommonOptionsPhq(MuniProcess *, const MuniCmdLineParser&);
  void CommonOutputMultiple(MuniProcess *, const MuniCmdLineParser&);
  void CommonOutputSingle(MuniProcess *, const MuniCmdLineParser&,
			  const wxString& =wxEmptyString);
  void EnvironmentPhCorr(MuniProcess *);
  void WriteOutput(MuniProcess *, const wxString&) const;
  void WriteFiles(MuniProcess *, const MuniCmdLineParser&) const;
  void WriteFiles(MuniProcess *, const wxArrayString&) const;
  void WriteFile(MuniProcess *, const wxString&) const;

  bool bias(MuniProcess *,MuniCmdLineParser&);
  bool dark(MuniProcess *,MuniCmdLineParser&);
  bool flat(MuniProcess *,MuniCmdLineParser&);
  bool phcorr(MuniProcess *,MuniCmdLineParser&);
  bool colouring(MuniProcess *,MuniCmdLineParser&);
  bool cone(MuniProcess *,MuniCmdLineParser&);
  bool astrometry(MuniProcess *,MuniCmdLineParser&);
  bool phcal(MuniProcess *,MuniCmdLineParser&);
  bool find(MuniProcess *,MuniCmdLineParser&);
  bool aphot(MuniProcess *,MuniCmdLineParser&);
  bool gphot(MuniProcess *,MuniCmdLineParser&);
  bool artificial(MuniProcess *,MuniCmdLineParser&);
  bool votable(MuniProcess *,MuniCmdLineParser&);
  bool fits(MuniProcess *,MuniCmdLineParser&);
  //  void calibrate(MuniProcess *,MuniCmdLineParser&);
  //  void list(MuniProcess *,MuniCmdLineParser&);
  bool timeseries(MuniProcess *,MuniCmdLineParser&);
  bool kombine(MuniProcess *,MuniCmdLineParser&);
  bool phfotran(MuniProcess *,MuniCmdLineParser&);
  bool cross(MuniProcess *,MuniCmdLineParser&);
  bool sesame(MuniProcess *,MuniCmdLineParser&);

  bool grow_report(MuniProcess *,MuniCmdLineParser&);

  void cone_lists(const MuniCmdLineParser&);

  void apstr(const wxString&, long&, wxString&);
  void dblstr(const wxString&, const wxString&, long&, wxString&);

  wxString GetAdvanced(const wxString&) const;
  wxString GetOutput(const wxString&) const;
  wxString GetFormat(size_t) const;

  void SetAdvanced(bool b) { advanced = b; }
  void SetPattern(const wxString& a) { pattern = a; }
  void SetMask(const wxString& a) { mask = a; }
  void SetFormat(const wxString& a) { format = a; }
  bool SetTargetDir(const wxString&);

  bool advanced;
  wxString pattern, mask, format, target;

  MuniPipe pipe;
};

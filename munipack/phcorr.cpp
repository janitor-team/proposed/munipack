/*

  Photometric corrections

  Copyright © 2012-3, 2016, 2018-9 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "munipack.h"


bool Munipack::phcorr(MuniProcess *action, MuniCmdLineParser& cmd)
{
  CommonOptionsBitpix(action,cmd);
  CommonOutputMultiple(action,cmd);
  CommonOptionsEnableOverwrite(action,cmd);
  EnvironmentPhCorr(action);

  wxString a;
  if( !  cmd.Found("O") &&
      ! (cmd.Found("target-directory",&a) || cmd.Found("t",&a)) ) {
    SetAdvanced(true);
    SetMask("\\1_proc.\\2");
  }

  if( cmd.Found("normalise-flat") )
    action->Write("NORMALISE = T");

  if( cmd.Found("flat",&a) )
    action->Write("FLAT = '"+a+"'");

  if( cmd.Found("bias",&a) )
    action->Write("BIAS = '"+a+"'");

  if( cmd.Found("dark",&a) )
    action->Write("DARK = '"+a+"'");

  if( cmd.Found("bitmask",&a) )
    action->Write("MASK = '"+a+"'");

  if( cmd.Found("xbitmask",&a) )
    action->Write("XMASK = '"+a.Upper()+"'");

  long i;
  if( cmd.Found("box",&i) )
    action->Write("BOX = %ld",i);

  double x;
  if( cmd.Found("xdark",&x) )
    action->Write("XDARK = %lf",x);

  if( cmd.Found("gain",&x) )
    action->Write("GAIN = %lf",x);

  WriteFiles(action,cmd);

  return true;
}

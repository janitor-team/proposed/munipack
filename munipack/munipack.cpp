/*

  Munipack - command line interface

  Copyright © 2010-2020 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "munipack.h"
#include <wx/wx.h>
#include <wx/app.h>
#include <wx/cmdline.h>

typedef bool (Munipack::*ActionFunction) (MuniProcess *,MuniCmdLineParser&);


wxIMPLEMENT_APP_CONSOLE(Munipack);

bool Munipack::OnInit()
{
  // switch-off timestamps in logs
  wxLog::DisableTimestamp();

  // default log level prints errors and warnings only
  wxLog::SetLogLevel(wxLOG_Warning);

  // setup variables
  advanced = false;
  pattern = "(.+)\\.(.+)";

  // recognize actions
  const char *actions[] = {
    "bias", "dark", "flat", "phcorr", "colouring",
    "cone", "astrometry", "phcal",
    "votable", "aphot", "gphot", "find", "timeseries", "fits",
    "kombine", "phfotran", "cross", "artificial", "sesame", 0
  };

  // define actions
  const ActionFunction funcs[] = {
    &Munipack::bias, &Munipack::dark, &Munipack::flat, &Munipack::phcorr,
    &Munipack::colouring, &Munipack::cone, &Munipack::astrometry,
    &Munipack::phcal, &Munipack::votable, &Munipack::aphot, &Munipack::gphot,
    &Munipack::find, &Munipack::timeseries, &Munipack::fits,
    &Munipack::kombine, &Munipack::phfotran, &Munipack::cross,
    &Munipack::artificial, &Munipack::sesame
  };

  int idxact = -1;
  wxString action;
  for(int i = 0; i < argc; i++)
    for(int j = 0; actions[j] != 0; j++)
      if( argv[i] == actions[j] ) {
	idxact = j;
	action = actions[j];
	goto fin;
      }
 fin:

  // process command-line
  MuniCmdLineParser cmd(action,argc,argv);
  OnInitCmdLine(cmd);

  Bind(wxEVT_END_PROCESS,&Munipack::OnFinish,this);
  pipe.SetHandler(this);

  if( cmd.Parse(false) == 0 ) {

    if( cmd.Found("verbose") )
      wxLog::SetLogLevel(wxLOG_Debug);

    if( action == "cone" &&
	(cmd.Found("list-catalogues") || cmd.Found("list-servers"))) {
      cone_lists(cmd);
      wxQueueEvent(this,new wxProcessEvent());
      pipe.SetExitCode(0);
      return true;
    }

    if( idxact != -1 ) {

      // commands to run (including exceptions)
      wxString com(action);
      if( action == "bias" ) com = "dark";

      // run
      MuniProcess *proc = new MuniProcess(&pipe,com);

      ActionFunction fun = funcs[idxact];
      if( (this->*fun)(proc,cmd) ) {

	pipe.push(proc);

	// grow-curve photometry report
	if( action == "gphot" && cmd.Found("verbose") ) {
	  MuniProcess *report = new MuniProcess(&pipe,"growreport");
	  grow_report(report,cmd);
	  pipe.push(report);
	}

	pipe.Start();
	return true;
      }
      else {
	delete proc;
	return false;
      }
    }
    else {

      if( cmd.Found("version") ) {
	wxPrintf("%s %s, %s\n",PACKAGE_NAME,PACKAGE_VERSION,PACKAGE_COPYLEFT);
	wxPrintf("%s\n\n",PACKAGE_DESCRIPTION);
	wxPrintf("This program comes with ABSOLUTELY NO WARRANTY;\nfor details, see the GNU General Public License, version 3 or later.\n");
      }
      else
	cmd.Usage();

      wxQueueEvent(this,new wxProcessEvent());
      pipe.SetExitCode(0);
      return true;
    }
  }
  else { // parse < 1 or parse == -1

    cmd.Usage();
    wxQueueEvent(this,new wxProcessEvent());
    pipe.SetExitCode(0);
    return true;
  }

  return false;
}

int Munipack::OnRun()
{
  wxAppConsole::OnRun();
  return pipe.GetExitCode();
}

void Munipack::OnFinish(wxProcessEvent& event)
{
  ExitMainLoop();
}

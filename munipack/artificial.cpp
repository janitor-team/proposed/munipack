/*

  Artificial frames

  Copyright © 2016-9 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "munipack.h"
#include "vocatconf.h"
#include <wx/wx.h>
#include <wx/filename.h>

using namespace std;


bool Munipack::artificial(MuniProcess *action, MuniCmdLineParser& cmd)
{
  CommonOptionsBitpix(action,cmd);

  double x,y;
  long w,h,n;
  wxString a, cname, filter;

  if( cmd.Found("verbose") )
    action->Write("VERBOSE = T");

  if( cmd.Found("phsystab",&a) )
    action->Write("PHSYSTABLE = '" + a + "'");
#ifdef MUNIPACK_DATA_DIR
  else {
    wxFileName fname(MUNIPACK_DATA_DIR,"photosystems.fits");
    action->Write("PHSYSTABLE = '"+fname.GetFullPath()+"'");
  }
#endif

  if( cmd.Found("psf",&a) )
    action->Write("PROFILE = '" + a.Upper() + "'");

  if( cmd.Found("spread",&a) )
    action->Write("SPREAD = '" + a.Upper() + "'");

  if( cmd.Found("hwhm",&x) )
    action->Write("HWHM = %lf",x);

  if( cmd.Found("eccentricity",&x) )
    action->Write("ECCENTRICITY = %lf",x);

  if( cmd.Found("inclination",&x) )
    action->Write("INCLINATION = %lf",x);

  if( cmd.Found("airy",&x) )
    action->Write("AIRY = %lf",x);

  if( cmd.Found("beta",&x) )
    action->Write("BETA = %lf",x);

  if( cmd.Found("maglim",&w) )
    action->Write("MAGLIM = %ld",w);

  if( cmd.Found("lc-model",&a) )
    action->Write("LCMODEL = '" + a.Upper() + "'");

  if( cmd.Found("lc-table",&a) )
    action->Write("LCTABLE = '" + a + "'");

  if( cmd.Found("lc-fourier",&a) )
    action->Write("LCFOURIER = '" + a + "'");

  if( cmd.Found("lc-mag",&x) )
    action->Write("LCMAG = %lf",x);

  if( cmd.Found("lc-amp",&x) )
    action->Write("LCAMP = %lf",x);

  if( cmd.Found("lc-jd0",&x) )
    action->Write("LCJD0 = %lf",x);

  if( cmd.Found("lc-per",&x) )
    action->Write("LCPER = %lf",x);

  if( cmd.Found("lc-ra",&x) && cmd.Found("lc-dec",&y) )
    action->Write("LCCOO = %20.15f %20.15f",x,y);

  if( cmd.Found("sky-mag",&x) )
    action->Write("SKYMAG = %lf",x);

  if( cmd.Found("sky-grad-x",&x) )
    action->Write("SKYGRADX = %lf",x);

  if( cmd.Found("sky-grad-y",&x) )
    action->Write("SKYGRADY = %lf",x);

  if( cmd.Found("area",&x) )
    action->Write("AREA = %lf",x);

  if( cmd.Found("diameter",&x) )
    action->Write("DIAMETER = %lf",x);

  if( cmd.Found("exptime",&x) )
    action->Write("EXPTIME = %lf",x);

  if( cmd.Found("qeff",&x) )
    action->Write("QEFF = %lf",x);

  if( cmd.Found("extk",&x) )
    action->Write("EXTINK = %lf",x);

  if( cmd.Found("timestep",&x) )
    action->Write("TIMESTEP = %lf",x);

  if( cmd.Found("count",&n) )
    action->Write("NFILES = %ld",n);

  if( cmd.Found("date",&a) ) {
    a.Replace("-"," ");
    action->Write("DATE = " + a);
  }

  if( cmd.Found("time",&a) ) {
    a.Replace(":"," ");
    action->Write("TIME = " + a);
  }

  if( cmd.Found("fov",&x) )
    action->Write("FOV = %lf",x);

  if( cmd.Found("f",&filter) || cmd.Found("filter",&filter) )
    action->Write("FILTER = '" + filter + "'");

  if( cmd.Found("width",&w) && cmd.Found("height",&h) )
    action->Write("NAXES = %ld %ld",w,h);

  if( cmd.Found("rcen",&x) && cmd.Found("dcen",&y) )
    action->Write("CRVAL = %20.15f %20.15f",x,y);

  if( cmd.Found("scale",&x) )
    action->Write("SCALE = %25.15e",x);

  if( cmd.Found("angle",&x) )
    action->Write("ANGLE = %20.15f",x);

  if( cmd.Found("long",&x) && cmd.Found("lat",&y) )
    action->Write("GEOGRAPHIC = %20.15f %20.15f",x,y);

  if( cmd.Found("photsys",&a) )
    action->Write("PHOTSYS = '"+a+"'");

  if( cmd.Found("atmosphere") )
    action->Write("ATMOSPHERE = T");

  if( cmd.Found("mask",&a) )
    action->Write("OUTPUT = '"+a+"'");

  if( cmd.Found("disable-noise") )
    action->Write("NOISE = F");

  if( cmd.Found("c",&cname) || cmd.Found("cat",&cname) ) {

      VOCatConf catalogs;
      VOCatResources cat(catalogs.GetCatFits(cname));

      if( cmd.Found("col-ra",&a) )
	action->Write("COL_RA = '" + a + "'");
      else if( cat.IsOk() )
	action->Write("COL_RA = '" + cat.GetLabel("POS_EQ_RA") + "'");

      if( cmd.Found("col-dec",&a) )
	action->Write("COL_DEC = '" + a + "'");
      else if( cat.IsOk() )
	action->Write("COL_DEC = '" + cat.GetLabel("POS_EQ_DEC") + "'");

      if( cmd.Found("col-pm-ra",&a) )
	action->Write("COL_PMRA = '" + a + "'");
      else if( cat.IsOk() )
	action->Write("COL_PMRA = '" + cat.GetLabel("POS_EQ_PMRA") + "'");

      if( cmd.Found("col-pm-dec",&a) )
	action->Write("COL_PMDEC = '" + a + "'");
      else if( cat.IsOk() )
	action->Write("COL_PMDEC = '" + cat.GetLabel("POS_EQ_PMDEC") + "'");

      if( cmd.Found("col-mag",&a) )
	action->Write("COL_MAG = '" + a + "'");
      else if( filter != "" )
	action->Write("COL_MAG = '" + filter + "mag'");
      else if( cat.IsOk() )
	action->Write("COL_MAG = '" + cat.GetSort() + "'");

      action->Write("CAT = '" + cname + "'");
  }

  return true;
}

/*

  Cross-match

  Copyright © 2013, 2018 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "munipack.h"
#include <wx/wx.h>

using namespace std;


bool Munipack::cross(MuniProcess *action, MuniCmdLineParser& cmd)
{
  CommonOutputSingle(action,cmd);

  wxString a,c;
  long n;
  double x;

  if( cmd.GetParamCount() != 2 )
    wxLogFatalError("Cross-match needs exactly two tables.");

  if( cmd.Found("tol",&x) )
    action->Write("TOL = %e",x);

  if( cmd.Found("ftol",&x) )
    action->Write("FTOL = %e",x);

  if( cmd.Found("col-ra",&a) ) {
    apstr(a,n,c);
    action->Write("COL_RA = " + c);
  }

  if( cmd.Found("col-dec",&a) ) {
    apstr(a,n,c);
    action->Write("COL_DEC = " + c);
  }

  if( cmd.Found("col-pm-ra",&a) ) {
    apstr(a,n,c);
    action->Write("COL_PMRA = " + c);
  }

  if( cmd.Found("col-pm-dec",&a) ) {
    apstr(a,n,c);
    action->Write("COL_PMDEC = " + c);
  }

  if( cmd.Found("col-mag",&a) ) {
    apstr(a,n,c);
    action->Write("COL_MAG = " + c);
  }

  WriteFiles(action,cmd.GetFiles());

  return true;
}

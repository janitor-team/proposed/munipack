/*

  Find Stars

  Copyright © 2013, 2016-8 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "munipack.h"
#include <wx/wx.h>

bool Munipack::find(MuniProcess *action, MuniCmdLineParser& cmd)
{
  CommonOutputMultiple(action,cmd);

  double x;

  if( cmd.Found("f",&x) || cmd.Found("fwhm",&x) )
    action->Write("FWHM = %lf",x);

  if( cmd.Found("th",&x) || cmd.Found("threshold",&x) )
    action->Write("THRESHOLD = %lf",x);

  if( cmd.Found("read-noise",&x) )
    action->Write("READNOISE = %lf",x);

  if( cmd.Found("saturate",&x) )
    action->Write("SATURATE = %lf",x);

  if( cmd.Found("lothresh",&x) )
    action->Write("LOWER_THRESHOLD = %lf",x);

  if( cmd.Found("shrplo",&x) )
    action->Write("SHARP_LOWER = %lf",x);

  if( cmd.Found("shrphi",&x) )
    action->Write("SHARP_HIGHER = %lf",x);

  if( cmd.Found("rndlo",&x) )
    action->Write("ROUND_LOWER = %lf",x);

  if( cmd.Found("rndhi",&x) )
    action->Write("ROUND_HIGHER = %lf",x);

  WriteFiles(action,cmd);

  return true;
}

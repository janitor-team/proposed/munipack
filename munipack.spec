Name: munipack
Summary: Munipack is a general astronomical image software package.
Version: X.X.X
Release: 1
License: GPL3
Group: Science
URL: http://munipack.physics.muni.cz
Source0: ftp://integral.physics.muni.cz/pub/%{name}/%{name}-%{version}.tar.gz
Requires: dcraw

%description
Munipack is a general astronomical image software package.
It provides both command line and graphical interfaces for processing
of huge sets of images. Currently implemented functions includes tools
for a basic reduction, aperture photometry, astrometry, matching and
composition of images. All tools are developed in mind of benefits
offered by robust statistical methods.

%prep
%setup

%build
%configure PATH="/usr/libexec/wxGTK3:$PATH" CXXFLAGS="-I/usr/include/cfitsio"
make

%install
%make_install

%package core
Summary: Munipack core
%description core
%files core
%defattr(-,root,root)
%{_libexecdir}/%{name}/aphot
%{_libexecdir}/%{name}/dark
%{_libexecdir}/%{name}/flat
%{_libexecdir}/%{name}/phcal
%{_libexecdir}/%{name}/pphot
%{_libexecdir}/%{name}/wcsremove
%{_libexecdir}/%{name}/wcsupdate
%{_libexecdir}/%{name}/astrometry
%{_libexecdir}/%{name}/cross
%{_libexecdir}/%{name}/find
%{_libexecdir}/%{name}/phcorr
%{_libexecdir}/%{name}/timeseries
%{_libexecdir}/%{name}/coloring
%{_libexecdir}/%{name}/ctrafo
%{_libexecdir}/%{name}/fits
%{_libexecdir}/%{name}/kombine
%{_libexecdir}/%{name}/phfotran
%{_datarootdir}/%{name}/ctable.dat
%{_datarootdir}/%{name}/photosystems.fits

%package cli
Summary: Munipack Command line interface
%description cli
%files cli
%defattr(-,root,root)
%{_bindir}/munipack
%{_mandir}/man1/munipack.1.gz
%{_libexecdir}/%{name}/votable
%{_libexecdir}/%{name}/cone
%{_datarootdir}/%{name}/VOcat_conf.xml

%package gui
Summary: Munipack  Graphical user interface
%description gui
%files gui
%defattr(-,root,root)
%{_bindir}/xmunipack
%{_mandir}/man1/xmunipack.1.gz
%{_datarootdir}/pixmaps/%{name}/*
%{_datarootdir}/applications/

%package doc
Summary: Munipack Documentation
%description doc
%files doc
%defattr(-,root,root)
%{_datarootdir}/doc/%{name}/*

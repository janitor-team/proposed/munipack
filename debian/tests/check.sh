
# exit imediatelly, when any command fails
set -e

FILE=barnard-calibrated.fits
CONE=cone_barnard.fits
# value of CTPH should be near 12 for $FILE
good=12

cp astrometry/${FILE} ${AUTOPKGTEST_TMP}
cp astrometry/${CONE} ${AUTOPKGTEST_TMP}
cd ${AUTOPKGTEST_TMP}

munipack find -f 5 $FILE
munipack aphot $FILE
# munipack cone -r 0.1 --par 'Vmag=<15' -o cone_barnard.fits -- 269.44 4.69
munipack astrometry -c $CONE $FILE
munipack phcal --photsys-ref Johnson --area 0.3 \
	 --col-ra RAJ2000 --col-dec DEJ2000 \
	 -f V --col-mag Vmag --col-magerr e_Vmag -c $CONE \
	 -O --mask phcal.fits $FILE

CTPH=$(munipack fits -K CTPH --value phcal.fits\[PHOTOMETRY\])

# log
echo $CTPH | awk -v good=$good '{ d=$1-good; if(d<0)d=-d; if(d<1) {print "OK",d;} else {print "FAIL",d;}}'

STATUS=$(echo $CTPH | awk -v good=$good '{ d=$1-good; if(d<0)d=-d; if(d<1) {print "OK";}}')

if [ "$STATUS" = "OK" ]; then
    exit 0
fi

exit 1

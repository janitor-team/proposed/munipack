
 !  Copyright © 2017 F.Hroch (hroch@physics.muni.cz)
 !
 !  This file is part of Munipack.
 !
 !  Munipack is free software: you can redistribute it and/or modify
 !  it under the terms of the GNU General Public License as published by
 !  the Free Software Foundation, either version 3 of the License, or
 !  (at your option) any later version.
 !
 !  Munipack is distributed in the hope that it will be useful,
 !  but WITHOUT ANY WARRANTY; without even the implied warranty of
 !  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 !  GNU General Public License for more details.
 !
 !  You should have received a copy of the GNU General Public License
 !  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

module lcparams  

   implicit none
   integer, parameter, private :: db = selected_real_kind(15)

   complex(db), dimension(0:7), parameter :: dcep = (/ &
     cmplx( 1.431827577603493E+00, 0.000000000000000E+00), &       
     cmplx( 1.523175784455409E-01, 1.104807514910180E-01), &       
     cmplx( 7.973120423394676E-02, 9.968514851191318E-03), &       
     cmplx( 3.152082966293240E-02,-1.391914783054839E-02), &       
     cmplx( 8.850460380123276E-03,-1.103153665740142E-02), &       
     cmplx( 1.193281021510767E-03,-6.004783684961505E-03), &       
     cmplx(-1.526078763221885E-03,-2.997269642313125E-03), &       
     cmplx(-2.299130275231806E-03,-1.055388540224690E-03) /)       

   complex(db), dimension(0:13), parameter :: algol = (/ &
     cmplx( 8.803129720011226E-01, 0.000000000000000E+00), &       
     cmplx(-8.754843862633307E-02,-1.807254358958903E-03), &       
     cmplx(-9.136932372740307E-02, 5.207577776709341E-03), &       
     cmplx(-6.759877186503378E-02,-5.338801947920397E-04), &       
     cmplx(-6.924649589371515E-02,-1.204029527445635E-03), &       
     cmplx(-5.468672536088746E-02,-1.953172654421660E-03), &       
     cmplx(-4.919355502992225E-02,-4.293251737271619E-03), &       
     cmplx(-3.626726111864739E-02,-1.324886630766237E-03), &       
     cmplx(-3.270027966343172E-02,-2.988705436844936E-03), &       
     cmplx(-2.014639016006056E-02, 4.760157699929647E-05), &       
     cmplx(-1.662215846504234E-02, 5.132462642119128E-04), &       
     cmplx(-9.663972853464540E-03,-1.907798084058917E-03), &       
     cmplx(-7.146719367978791E-03, 1.024170933908850E-03), &       
     cmplx(-1.968336366794583E-03, 1.011493681013776E-03) /)       

end module lcparams  

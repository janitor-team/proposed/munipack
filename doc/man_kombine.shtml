<!DOCTYPE HTML>
<html lang="en">
<head>
<!-- #include virtual="/meta.shtml" -->
<title>Munipack ‒ Kombine</title>
</head>
<body>
<!-- #include virtual="/head.shtml" -->
<section>

<h1>Kombine</h1>

<p class="abstract">
  Kombine composes multiple frames into a new frame. This routine
  provides stacking of multiple frames as well as assembling
  of a mosaic.
</p>

<h2>Command</h2>

<code>
munipack kombine [..] files
</code>

<h2>Description</h2>

<p>
  Purpose of this routine is to collect observed frames together
  to reveal very faint objects (potentially invisible on a particular frame),
  to increase of the dynamic range of frames,
  to average frames or to create of a mosaics
  covering larger sky area than any single frame.
</p>

<p class="indent">
  The processing of frames follows the way: Every input frame
  is re-projected on the sky in a grid while its nodes are interpolated.
  Values for every node are averaged. The output frame is created as
  a projection of the sky nodes onto plane of a new synthetic frame.
</p>

<p class="indent">
  The processing is relative flexible and has advantageous
  characteristics. Result image can be scaled to arbitrary size,
  rotated, shifted and reflected.
  The routine provides sub-pixel resolution. The blurring of image due to
  convolution can be reduced by choose interpolation method.
</p>
<p class="indent">
  The output frames can be recommended
  generally for a regular photometry except photometry of background, because
  different levels of particular images are removed during processing.
</p>

<h2>Input And Output</h2>

<p>
  On input, list of frames with astrometry calibration is necessary.
  A regular photometry calibration is recommended for averaging.
</p>

<p>
  On output, a new FITS frame is created.
</p>


<h2>Memory allocation</h2>

<p>
  Kombine requires approximately NAXIS(1)*NAXIS(2)*4 bytes per frame
  (frames are stored as single precision real numbers).
  For an illustration, one hundredth  of 1000 x 1000 frames (1MB)
  takes 400 MB in memory. Some additional memory (commonly four
  frames) is also required.
</p>

<p class="indent">
  The robust mean mode requires twice more of memory per frame
  which means that 800 MB of memory will be required in the above case.
</p>

<h2>Parameters</h2>

<h3>Parameters for composition:</h3>
<dl>
  <dt><samp>-i, --interpol [near,bilinear,bicubic,bi3conv]</samp></dt>
  <dd>An interpolation method selection:
    <a href="https://en.wikipedia.org/wiki/Image_scaling">nearest
      neighbourhood</a>,
    <a href="https://en.wikipedia.org/wiki/Bilinear_interpolation">bilinear</a>,
    <a href="https://en.wikipedia.org/wiki/Bicubic_interpolation">bicubic</a>
    (default) and
    <a href="https://en.wikipedia.org/wiki/Bicubic_interpolation">bi3conv
      by convolution</a>.

    The plain bi-cubic interpolation is preferred against to other variants
    because it gives smooth and appropriate images. The variant of bi-cubic
    convolution can give better results for critically under-sampled frames.
    The nearest neighbourhood method gives low quality results
    in a minimal time.
  </dd>
  <dt><samp>--disable-back</samp></dt><dd>
    Switch-off subtract of the estimated background. As the result,
    the frames has properly scaled background including sky brightest;
    any composed frames, especially mosaics, will show white and black
    regions.
  </dd>
  <dt><samp>--arith</samp></dt><dd>
    Compute average by arithmetic mean for every single pixel of output.
    By default, average is computed by robust mean giving brilliant
    look of result, but which is significantly slow and memory consuming.
  </dd>
</dl>

<h3>Geometry parameters:</h3>
<dl>
  <dt><samp>-p, --projection [none, gnomonic]</samp></dt>
  <dd> projection: none, gnomonic (default)</dd>
  <dt><samp>--rcen ddd.ddd, --dcen ddd.ddd</samp></dt>
  <dd> centre of projection in Right Ascension and Declination [deg]</dd>
  <dt><samp>--pm-ra d.ddd, --pm-dec d.ddd</samp></dt><dd> proper motion
    of centre of projection in [deg/day]</dd>
  <dt><samp>--pm-jdref JD</samp></dt><dd> reference Julian date for
    the proper motion</dd>
  <dt><samp>--width xxx, --height yyy</samp></dt><dd>dimensions of the output
    frame in pixels</dd>
  <dt><samp>--xcen xxx.x, --ycen yyy.y</samp></dt>
  <dd> centre of projection on result [pix] (default: width/2, height/2)</dd>
  <dt><samp>--scale sss.sss</samp></dt><dd>scale [deg/pix]</dd>
  <dt><samp>--angle ddd.ddd</samp></dt><dd>rotation angle [deg]</dd>
  <dt><samp>--reflex [yes|no]</samp></dt><dd>setup reflection</dd>
</dl>

<p>
  Unspecified parameters are inherited from the first
  frame on input. The strategy is chosen as the way of minimal surprise.
</p>

<p>
  Exotic mounts of a camera device can be misleading,
  the live will more easy with set of the zero rotation
  angle (<samp>--angle 0</samp>) and with
  no reflection (<samp>--reflex no</samp>) options.
</p>

<p>See <a href="man_com.html">Common options</a> for input/output filenames.</p>

<h2>Examples</h2>

<p>
  The very basic and common way of use is like
</p>
<code>
$ munipack kombine images*.fits
</code>
<p>
  All parameters of output frame (saved to <samp>kombine.fits</samp>)
  are inherited from the first frame.
</p>

<p>
  There a way how to save the result under specified filename
  which has main axis oriented by a conventional way
</p>
<code>
$ munipack kombine -o kombi.fits --angle 0 --reflex no  images*.fits
</code>

<p>
  The frames can be composed into a large canvas 1000×1000 pixels
  and scaled by factor two. As the first step, one derives the frame
  scale by FITS keywords
</p>
<pre>
$ munipack fits -K CD1_1,CD1_2 image1.fits
CD1_1   = -2.6589961865059603E-04
CD1_2   = -6.0182917772180531E-06
</pre>
<p>
  The frame scale is <i>√({CD1_1}<sup>2</sup> + {CD1_2}<sup>2</sup>)</i>
  = 2.66e-4 deg/pix. The parameters are
</p>
<code>
$ munipack kombine --width 1000 --height 1000 --scale 5.32e-4  images*.fits
</code>

<p>
  A little bit unusual usage of composition is for tracking
  of comets or faint asteroids:
</p>
<code>
$ munipack kombine --pm-ra 0.4 --pm-dec -0.01 images*.fits
</code>
<p>
  The utility sets initial frame position and time by the first
  image and shifts all next images by the specified proper motion.
  Best way how to get values of the proper motion is to use of its
  ephemeris. An alternative, for visible objects, is the difference
  of object coordinates between the first and last images.
</p>

<h2>See Also</h2>
<p>
  <a href="kombitut.html">Frame Composition</a>
  is detailed tutorial how to get a deep exposure while,
  <a href="mosaics.html">Mosaics</a> is detailed tutorial how to
  assembly a mosaic.
  <a href="colorimage.html">Frame Colouring</a>
  uses kombine to get more deep frames with nice example
  of use <samp>--rcen, --dcen</samp> parameters.

</p>
<p>
  See also:
  <a href="https://en.wikipedia.org/wiki/Slip_(clothing)"
     title="Kombiné is Czech term for slip ☺">Slip</a>,
  <a href="man_com.html">Common options</a>
</p>

</section>
<!-- #include virtual="/foot.shtml" -->
</body>
</html>

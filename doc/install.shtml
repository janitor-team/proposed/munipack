<!DOCTYPE HTML>
<html lang="en">
<head>
<!-- #include virtual="/meta.shtml" -->

<title>Munipack ‒ Installation</title>
</head>
<body>
<!-- #include virtual="/head.shtml" -->
<section>

<h1 class="noindent">Installation</h1>

<p class="abstract">
  This page reviews alternatives to install of Munipack.
</p>

<h2>Munipack in Debian and Ubuntu</h2>

<p>
<img class="symbol" src="big_logo.png" width="64" height="64" alt="Munipack logo">
</p>

<p>
  Munipack is available as an official package repository in Debian (since
  June 2017) and Ubuntu (April 2017).
  Regular issues of (these) distributions are usually delayed behind
  Munipack releases itself.
</p>

<p>
  The installation is straightforward by standard software tools.
  Packages can be found in the appropriate repository:
  <a href="https://packages.debian.org/munipack">Debian</a>,
  <a href="http://packages.ubuntu.com/munipack">Ubuntu</a>.
  </p>


<h2><a id="ubuntu">Ubuntu 16.04 (Xenial Xerus) and 18.04 (Bionic Beaver)</a></h2>

<p>
<img class="symbol" src="ubuntu_icon.png" width="54" height="55" alt="ubuntu icon">
</p>

<p>
  There are temporary packages prepared for some older Ubuntu releases.
  The installation under Ubuntu 16.04 (Xenial Xerus)
  and 18.04 (Bionic Beaver)
  requires appending of the suitable line to
<samp>/etc/apt/sources.list</samp>:</p>
<pre>
deb https://munipack.physics.muni.cz/ubuntu/xenial xenial main
deb https://munipack.physics.muni.cz/ubuntu/bionic bionic main
</pre>

<p>
  The system package list should to import sign key, to be updated, and
  Munipack can be installed:
</p>
<pre>
$ wget -O - https://integral.physics.muni.cz/~hroch/hroch@physics.muni.cz.gpg.key | \
  sudo apt-key add -
$ sudo apt-get update
$ sudo apt-get install munipack
</pre>


<h2><a id="development">Development repository for Debian</a></h2>

<p>
<img class="symbol" src="debian_icon.png" width="64" height="64" alt="debian icon">
</p>


<p>There is a bleeding edge branch of Munipack, which is primary intended
  for testing purposes.
  The latest development packages are usually available only for stable
  64-bit Debian.
</p>
<p>
The repository can be activated by including the line
</p>
<pre>
deb https://munipack.physics.muni.cz/devel/ stable main
</pre>
<p>
  into <samp>/etc/apt/sources.list</samp>
  and importing GPG key:
  </p>
<pre>
$ wget -O - https://integral.physics.muni.cz/~hroch/hroch@physics.muni.cz.gpg.key| \
  sudo apt-key add -
</pre>

<p>The installation itself is straightforward:
</p>
<pre>
# apt-get update
# apt-get install munipack
</pre>

<p>
  Be warned, the development release should contain various problems and errors,
  outdated documentation, etc.
  If needed, anything can be corrected in the source package. Add also the line
  </p>
<pre>
deb-src https://munipack.physics.muni.cz/devel/ stable main
</pre>
<p>
  to <samp>/etc/apt/sources.list</samp>, update, install the source package
  and follow instructions described by
  <a href="https://debian-handbook.info/browse/stable/debian-packaging.html">The Debian Administrator's Handbook</a>.
</p>


<h2>Source code and bundle install</h2>

<p>
  Munipack is designed as multiplatform software; it
  can be compiled under many computer systems. The
  building from the <a href="SourceInstallation.html">source code</a>
  is the most portable way.
</p>

<p>
  Packages for <a href="debbuilder.html">DEB</a> and
  <a href="rpmbuilder.html">RPM</a> base distributions can be prepared.
</p>

<!--
<p>
Particularly, bundle (all-in-one) installation
for <a href="debbundle.html">DEB based distributions</a>,
<a href="rpmbundle.html">RPM based distributions</a>
can be also very helpful.
</p>
-->


<h2>See Also</h2>
<p>
<a href="download.html">Download</a>,
   <a href="version.html">Versions</a> and <a href="guide.html">Guide</a>.
</p>

</section>
<!-- #include virtual="/foot.shtml" -->
</body>
</html>

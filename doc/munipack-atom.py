#!/usr/bin/env python
#
# http://sluggo.scrapping.cc/python/WebHelpers/modules/feedgenerator.html

import sys,datetime,uuid

try:
   import webhelpers.feedgenerator as fd
except ImportError:
    sys.stderr.write("munipack-atom.py warning: import webhelpers failed.\n "+
                     "If you want generate RSS, please install python package `webhelpers' and try again (else this warning can be safety ignored).\n")
    sys.exit(1)


class record:
    def __init__(self,v,l,d,y,m,dd):
        self.version=v
        self.link=l
        self.description=d
        self.date=datetime.date(y,m,dd)

title = "Munipack"
link = "https://munipack.physics.muni.cz/"
description = "A general astronomical image processing software"

feed = fd.Atom1Feed(title,link,description,author_name="Filip Hroch",
                    author_email="hroch@physics.muni.cz")

records = [
   record("0.5.0",link,"Start of New Generation",2010,02,10),
   record("0.5.1",link,"Coloring release",2010,10,17),
   record("0.5.2",link,"Astrometry release",2011,10,19),
   record("0.5.3",link,"New generation release",2012,06,29),
   record("0.5.4",link,"Photon Rain",2013,07,31),
   record("0.5.5",link,"Nights Without Convergence",2014,04,01),
   record("0.5.6",link,"Photon Calibration",2015,01,05),
   record("0.5.7",link,"Growth Curve",2016,10,25),
   record("0.5.8",link,"Artificial Sky",2017,07,14),
   record("0.5.9",link,"Artificial Sky",2017,07,18),
   record("0.5.10",link,"Flat-fielding",2018,01,16),
   record("0.5.11",link,"Oak Leaf",2019,01,22),
   record("0.5.12",link,"Bullseye",2020,11,30),
   record("0.5.13",link,"Bullseye",2020,12,21),
   record("0.5.14",link,"Bullseye",2021,02,04)
]

for r in records:
   d = str(r.date)
   u = "urn:uuid:" + str(uuid.uuid3(uuid.NAMESPACE_URL,r.link + "," + d))
   feed.add_item(title+" "+r.version,r.link,r.description,pubdate=r.date,
                 unique_id=u)

feed.write(sys.stdout, 'utf-8')

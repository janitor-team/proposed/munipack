
# prepare data
wget ftp://integral.physics.muni.cz/pub/munipack/munipack-data-m51.tar.gz
tar zxf munipack-data-m51.tar.gz
cd munipack-data-m51/

# photometric corrections
export FITS_KEY_TEMPERATURE="CCD-TEMP"
munipack dark -o dark120.fits dark_005?.fits dark_008?.fits
munipack dark -o dark10.fits dark_003?.fits dark_004?.fits
munipack phcorr -dark dark10.fits flat_Green_*.fits
munipack flat -o flat_Green.fits flat_Green_*.fits
munipack phcorr -dark dark120.fits -flat flat_Green.fits m51_Green_*.fits

# find stars, aperture photometry
munipack find -f 7 -th 7  m51_Green_*_proc.fits
munipack aphot m51_Green_*_proc.fits

# astrometry
munipack cone -r 0.2 -- 202.47 +47.2
munipack astrometry m51_Green_00*_proc.fits

# kombine
munipack kombine --rcen 202.47 --dcen +47.2 --width 1000 --height 1000 \
	 -o m51.fits m51_Green_00??_proc.fits

# update filters, photometric calibration
for A in m51_Green_00??_proc.fits; do
    munipack fits --update --key FILTER --val "'V'" $A
    munipack phcal --photsys-ref Johnson --area 0.3 -c cone.fits \
           --col-ra RAJ2000 --col-dec DEJ2000 -f V --col-mag Vmag $A
done

# kombine of calibrated frames
munipack kombine --rcen 202.47 --dcen +47.2 --width 1000 --height 1000 \
	 -o m51_cal.fits m51_Green_00??_proc_cal.fits

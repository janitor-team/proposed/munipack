/*

  xmunipack - FITS open, export threads


  Copyright © 2019 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "xmunipack.h"
#include <wx/wx.h>

using namespace std;

// --- FitsOpen -------------------

FitsOpen::FitsOpen(wxEvtHandler *h, const wxString& fn, int is):
  wxThread(wxTHREAD_DETACHED),handler(h),filename(fn),icon_size(is)
{
  wxASSERT(handler);
}

FitsOpen::~FitsOpen()
{
  wxCriticalSectionLocker enter(static_cast<MuniView *>(handler)->loaderCS);
  static_cast<MuniView *>(handler)->loader = 0;
}

wxThread::ExitCode FitsOpen::Entry()
{
  // load FITS file
  FitsFile fits(filename);

  vector<FitsTone> tones;
  vector<wxImage> icons;

  if( fits.IsOk() ) {

    // for image HDU, estimate stat. parameters of all HDUs
    for(size_t k = 0; k < fits.HduCount(); k++) {
      if( fits.Hdu(k).Type() == HDU_IMAGE ) {
	FitsArray array(fits.Hdu(k));
	int idx;
	if( array.Flavour() == HDU_IMAGE_COLOUR )
	  idx = 1;
	else if( array.Flavour() == HDU_IMAGE_FRAME )
	  idx = 0;
	else
	  idx = -1;

	if( idx >= 0 ) {

	  // estimate Tone
	  FitsTone tone(array.Plane(idx));

	  // generate icons
	  FitsImage image(array);
	  FitsDisplay display(image);
	  display.SetShrink(icon_size,icon_size);
	  display.SetTone(tone);
	  display.SetOperations(OP_TUNE_ZOOM | OP_TUNE_SCALE | OP_TUNE_RGB);
	  FitsBitmap b = display.GetImage();
	  wxImage icon(b.GetWidth(),b.GetHeight(),b.NewTopsyTurvyRGB());
	  tones.push_back(tone);
	  icons.push_back(icon);
	}
      }
      else {
	tones.push_back(FitsTone());
	icons.push_back(wxImage());
      }
    }
  }

  FitsOpenEvent ev(EVT_FITS_OPEN,ID_LOADER);
  ev.filename = filename;
  ev.fits = fits;
  ev.icons = icons;
  ev.tones = tones;
  wxQueueEvent(handler,ev.Clone());

  return (wxThread::ExitCode) 0;
}



// --- FitsExport -------------------

FitsExport::FitsExport(wxEvtHandler *h, const FitsArray& a,
		       const wxString& s,const FitsTone& t,
		       const FitsItt& i, const FitsPalette& p,
		       const FitsColor& c):
  wxThread(wxTHREAD_DETACHED),handler(h),array(a),savename(s),tone(t),itt(i),
  pal(p),colour(c)
{
  wxASSERT(handler);
}

FitsExport::~FitsExport()
{
  wxCriticalSectionLocker enter(static_cast<MuniView *>(handler)->loaderCS);
  static_cast<MuniView *>(handler)->loader = 0;
}

wxThread::ExitCode FitsExport::Entry()
{
  FitsDisplay display(array);
  display.SetPalette(pal);
  display.SetItt(itt);
  display.SetColor(colour);
  display.SetTone(tone);

  FitsBitmap fitsbmp(display.GetImage());
  wxImage image(fitsbmp.GetWidth(),fitsbmp.GetHeight(),
		fitsbmp.NewTopsyTurvyRGB());
  image.SaveFile(savename);
  return (wxThread::ExitCode) 0;
}

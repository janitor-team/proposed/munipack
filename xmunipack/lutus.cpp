/*

  xmunipack - tune panel colour look-up table (LUT)

  Copyright © 2018-20 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "tune.h"
#include <wx/wx.h>
#include <wx/dcbuffer.h>
#include <algorithm>

#define WIDTH 256
#define STICK 20

using namespace std;

// ---- MuniLUTus  ---------------------------------------------

MuniLUTus::MuniLUTus(wxWindow *w): wxPanel(w,wxID_ANY,wxDefaultPosition,
					   wxSize(WIDTH,3*STICK))
{
  SetBackgroundStyle(wxBG_STYLE_PAINT);

  Bind(wxEVT_PAINT,&MuniLUTus::OnPaint,this);
}

void MuniLUTus::SetPalette(const wxString& l)
{
  pal.SetPalette(l);
  Refresh();
}

void MuniLUTus::SetPalette(const FitsPalette& p)
{
  pal = p;
  Refresh();
}

void MuniLUTus::SetInversePalette(bool b)
{
  pal.SetInverse(b);
  Refresh();
}

void MuniLUTus::OnPaint(wxPaintEvent& event)
{
  wxPaintDC dc(this);
  dc.SetPen(wxNullPen);

  int n = pal.GetColors();
  int dx = 1;
  int dy = STICK;
  int xoff = wxMax((dc.GetSize().GetWidth() - WIDTH)/2,0);

  n--;
  for(int j = 0; j < 3; j++)
    for(int i = 0; i < WIDTH; i++) {
      dc.SetBrush(wxColor(pal.R(n),pal.G(n),pal.B(n)));
      dc.DrawRectangle(xoff+i*dx,j*dy,dx,dy);
      n--;
    }
}

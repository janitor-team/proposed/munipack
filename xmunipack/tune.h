/*

  xmunipack - common for View & Tune


  Copyright © 1997-2009, 2018-9 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/


#ifndef _XMUNIPACK_TUNE_H
#define _XMUNIPACK_TUNE_H

#include "fits.h"
#include "event.h"
#include <wx/wx.h>
#include <wx/notebook.h>
#include <wx/spinctrl.h>
#include <vector>

class MuniLUTus: public wxPanel
{
public:
  MuniLUTus(wxWindow *);

  void SetPalette(const FitsPalette&);
  void SetPalette(const wxString&);
  void SetInversePalette(bool);

private:

  FitsPalette pal;

  void OnPaint(wxPaintEvent&);
};


class MuniMiniDisplay: public wxWindow
{
private:

  int width, height;
  wxBitmap bitmap;

  void OnPaint(wxPaintEvent&);

public:

  MuniMiniDisplay(wxWindow *, int, int);
  wxSize DoGetBestSize() const;
  void SetImage(const wxImage&);
};



class MuniTuneAdjusterBase: public wxControl
{
  void Entry(const wxString&);
  void OnScroll(wxScrollEvent&);
  void OnScrollFinish(wxScrollEvent&);
  void OnEntry(wxCommandEvent&);
  void OnEntryFinish(wxCommandEvent&);
  void OnSpinDouble(wxSpinDoubleEvent&);

 protected:

  double factor;
  wxString textentry;
  double value;
  wxSlider *slider;
  wxSpinCtrlDouble *entry;

  void Create(int,int,int,double,double,double,double,
	      unsigned int,long,const wxString&, const wxString&);

  virtual void OnUpdateValue(int) = 0;
  virtual void SetSlider(double) = 0;

 public:

  MuniTuneAdjusterBase(wxWindow *,wxWindowID =wxID_ANY);

  void SetToolTip(const wxString&);
  virtual void SetValue(double) = 0;

};

class MuniTuneAdjuster: public MuniTuneAdjusterBase
{
  virtual void OnUpdateValue(int);
  virtual void SetSlider(double);

public:
  MuniTuneAdjuster(wxWindow *,wxWindowID =wxID_ANY,double =0.0,double =0.0,
		   double =100.0, double =1.0, unsigned int =0,
		   const wxString& =wxEmptyString,
		   const wxString& =wxEmptyString);

  virtual void SetValue(double);

};

class MuniTuneLogjuster: public MuniTuneAdjusterBase
{
  virtual void OnUpdateValue(int);
  virtual void SetSlider(double);


public:
  MuniTuneLogjuster(wxWindow *,wxWindowID =wxID_ANY,double =0.0,double =0.0,
		    double =100.0, double =1.0, unsigned int =0,
		    const wxString& =wxEmptyString,
		    const wxString& =wxEmptyString);


  virtual void SetValue(double);

};


class MuniTune: public wxFrame
{
public:
  MuniTune(wxWindow *, wxWindowID, const wxPoint&, const wxSize&, int,
	   const FitsArray&, const FitsTone&, const FitsItt&,
	   const FitsPalette&);
  MuniTune(wxWindow *, wxWindowID, const wxPoint&, const wxSize&, int,
	   const FitsArray&, const FitsTone&, const FitsItt&, const FitsColor&);

  virtual ~MuniTune();

private:

  FitsTone tone;
  FitsItt itt;
  FitsPalette pal;
  FitsColor color;
  FitsBaseDisplay *display;
  MuniMiniDisplay *mini;
  bool shrinking, colouring;
  bool toneabs, render, ittline, nitevision;

  wxNotebookPage *chpanel;
  wxChoice *type_itt;
  wxCheckBox *invcheck, *nitecheck;
  wxRadioButton *radio_abs, *radio_rel;
  MuniLUTus *lutus;
  wxChoice *lutch;
  MuniTuneAdjuster *adjblack,*adjamount,*adjzero,
    *adjsatur,*adjnthresh,*adjmeso,*adjhuee;
  MuniTuneLogjuster *adjsense;
  MuniTuneEvent *etune;

  void InitMini(const FitsArray&, int, int *, int *);
  void SetScroll(int,double);
  void OnIdle(wxIdleEvent&);
  void OnScroll(wxScrollEvent&);
  void OnScrollFinish(wxScrollEvent&);
  void OnChoiceItt(wxCommandEvent&);
  void OnChoicePal(wxCommandEvent&);
  void OnCheckInverse(wxCommandEvent&);
  void OnCheckNite(wxCommandEvent&);
  void OnRadioItt(wxCommandEvent&);
  void OnClose(wxCloseEvent&);
  void OnReset(wxCommandEvent&);
  void OnTuneFine(MuniTuneEvent&);

  void OnUpdateIttpar(wxUpdateUIEvent&);
  void OnUpdateNiteadj(wxUpdateUIEvent&);

  MuniTuneAdjuster *CreateToneBlack(wxWindow *);
  MuniTuneLogjuster *CreateToneSense(wxWindow *);
  MuniTuneAdjuster *CreateIttAmount(wxWindow *);
  MuniTuneAdjuster *CreateIttZero(wxWindow *);
  wxNotebookPage *CreateScaleTab(wxNotebook *);
  wxNotebookPage *CreateIttTab(wxNotebook *);
  wxNotebookPage *CreateLutTab(wxNotebook *);
  wxNotebookPage *CreateColourTab(wxNotebook *);
  wxNotebookPage *CreateNiteTab(wxNotebook *);

};

#endif

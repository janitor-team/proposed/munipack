/*

  Art Icons

  Copyright © 2019 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "mconfig.h"
#include <wx/wx.h>
#include <wx/artprov.h>


MuniArtIcons::MuniArtIcons(const wxArtClient& c, const wxSize& s):
  client(c), size(s) {}

wxBitmap MuniArtIcons::Icon(const wxArtID& id) const
{
#ifdef __WXMAC__
  wxBitmap b = IncludedIcon(id);
#else
  wxBitmap b = wxArtProvider::GetBitmap(id,client,size);
  if( ! b.IsOk() )
    b = IncludedIcon(id);
#endif

  if( b.IsOk() )
    return b;
  else
    return wxArtProvider::GetBitmap(wxART_MISSING_IMAGE,client,size);
}



wxBitmap MuniArtIcons::IncludedIcon(const wxArtID& id) const
{
  wxString name;

  if( id == "zoom-fit-best" )
    name = "stock_zoom_fit_width";
  else if( id == "zoom-original" )
    name = "stock_zoom_one_to_one";
  else if( id == "preferences-desktop" )
    name = "stock_preferences";
  else if( id == "wxART_GO_UP" )
    name = "stock_up";
  else if( id == "wxART_GO_HOME" )
    name = "stock_home";
  else if( id == "wxART_GO_BACK" )
    name = "stock_back";
  else if( id == "wxART_GO_FORWARD" )
    name = "stock_forward";
  else if( id == "wxART_FOLDER" )
    name = "stock_folder";
  else if( id == "stop" )
    name = "stock_cancel";
  else if( id == "exec" )
    name = "gtk-execute";
  else if( id == "save" )
    name = "document_save";
  else if( id == "refresh" )
    name = "stock_refresh";

  wxString fullpath = MuniConfig::FindIconPath(name+".png");
  if( ! fullpath.IsEmpty() ) {

    wxLogDebug("MuniArtIcons::Icon "+fullpath);
    wxImage icona(fullpath);
    if( icona.Ok() )
      return wxBitmap(icona);
  }

  return wxNullBitmap;
}

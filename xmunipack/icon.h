/*

  xmunipack - config

  Copyright © 2012, 2018-9 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef _XMUNIPACK_ICON_H_
#define _XMUNIPACK_ICON_H_

#include "fits.h"


class MuniIcon
{
public:
  MuniIcon(const FitsFile&, const wxString&, const wxString&,
	   const wxImage&, const wxImage&, const wxImage&, int);
  MuniIcon(const FitsFile&, const wxString&, const wxString&,
	   const wxImage&, const wxImage&, const wxImage&, int,
	   const std::vector<wxImage>&);
  virtual ~MuniIcon();

  wxImage GetIcon() const;
  std::vector<wxImage> GetList() const;
  wxImage GrayIcon(const FitsArray&, const FitsTone&) const;
  wxImage ColorIcon(const FitsArray&, const FitsTone&, const FitsColor&) const;
  wxImage ImageIcon(const FitsArray&) const;
  wxImage ImageIcon(const wxImage&) const;
  wxImage MultiIcon(const std::vector<wxImage>&) const;

  static wxImage BrowserIcon(const wxImage&,int,int,
			     const wxString& =wxEmptyString,
			     const wxColour& =*wxWHITE);
  static wxImage DefaultIcon(int,int);
  static wxImage SymbolIcon(const wxImage&,int,int);
  static wxImage ListIcon(const wxImage&,int,
			  const wxColour& =wxColour(255,255,255));
  static wxImage BulletIcon(const wxSize&, const wxColour&);

private:

  const FitsFile fits;
  wxImage icon;
  std::vector<wxImage> list;
  wxString display_colorspace,cdatafile;
  wxImage default_icon, table_icon, head_icon;
  int icon_size;

  wxImage Padding(const wxImage&,int) const;
};

#endif

/*

  XMunipack - derived events

  Copyright © 2009-2012, 2017-9 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/


#ifndef _XMUNIPACK_EVENT_H_
#define _XMUNIPACK_EVENT_H_

#include "types.h"

/*

  Definition of new events. Adds some data structures to standard ones.

*/



wxDECLARE_EVENT(EVT_CONFIG_UPDATED, wxCommandEvent);
wxDECLARE_EVENT(EVT_FILELOAD, wxCommandEvent);
wxDECLARE_EVENT(EVT_FINISH_DIALOG, wxCommandEvent);
wxDECLARE_EVENT(EVT_TOOL_FINISH, wxCommandEvent);



// -- Slew ---
// positions of zoomed areas

class MuniSlewEvent: public wxEvent
{
 public:
 MuniSlewEvent(wxEventType eventType =wxEVT_NULL, int id =wxID_ANY):
   wxEvent(id, eventType),x(0),y(0),inside(false),leaving(false),
   entering(false) {}
  virtual wxEvent *Clone(void) const { return new MuniSlewEvent(*this); }
  int x,y;
  bool inside, leaving, entering;
};

wxDECLARE_EVENT(EVT_SLEW,MuniSlewEvent);

#define MuniSlewEventHandler(func) (&func)

// -- Mouse click (warps mouse events which are not propagated)

class MuniClickEvent: public wxCommandEvent
{
 public:
 MuniClickEvent(wxEventType eventType =wxEVT_NULL, int id =wxID_ANY):
  wxCommandEvent(eventType,id),x(0),y(0),r(0.0) {}
  // MuniClickEvent(wxCommandEvent& e): wxCommandEvent(e) { }
  virtual wxEvent *Clone(void) const { return new MuniClickEvent(*this); }
  int x,y;
  double r;
};

wxDECLARE_EVENT(EVT_CLICK,MuniClickEvent);

#define MuniClickEventHandler(func) (&func)


// -- Tune

class MuniTuneEvent: public wxCommandEvent
{
 public:
  MuniTuneEvent(WXTYPE e =wxEVT_NULL, int id =wxID_ANY):
    wxCommandEvent(e,id),x(0.0),n(0),index(0) {}
  MuniTuneEvent(wxCommandEvent& e): wxCommandEvent(e),x(0.0),n(0),index(0) {}
  wxEvent *Clone(void) const { return new MuniTuneEvent(*this); }
  double x;
  int n;
  int index;
};

wxDECLARE_EVENT(EVT_TUNE,MuniTuneEvent);

#define MuniTuneEventHandler(func) (&func)

// -- Full Tune

class MuniFullTuneEvent: public wxCommandEvent
{
 public:
 MuniFullTuneEvent(WXTYPE e=wxEVT_NULL,int id=wxID_ANY): wxCommandEvent(e,id) {}
 MuniFullTuneEvent(wxCommandEvent& e): wxCommandEvent(e) {}
  wxEvent *Clone(void) const { return new MuniFullTuneEvent(*this); }
  FitsTone tone;
  FitsItt itt;
  FitsPalette pal;
  FitsColor colour;
};

wxDECLARE_EVENT(EVT_FULLTUNE,MuniFullTuneEvent);

#define MuniFullTuneEventHandler(func) (&func)

// -- Astrometry

class MuniAstrometryEvent: public wxCommandEvent
{
 public:
  MuniAstrometryEvent(WXTYPE e =wxEVT_NULL, int id =wxID_ANY):
  wxCommandEvent(e,id), scale(0.0),reflex(1.0),angle(0.0),xcen(0.0),
    ycen(0.0),acen(0.0),dcen(0.0),astrometry(false) {}
 MuniAstrometryEvent(wxCommandEvent& e):
  wxCommandEvent(e), scale(0.0), reflex(1.0),angle(0.0),xcen(0.0),ycen(0.0),
    acen(0.0),dcen(0.0),astrometry(false) {}
  wxEvent *Clone(void) const { return new MuniAstrometryEvent(*this); }
  wxString proj;
  double scale,reflex,angle,xcen,ycen,acen,dcen;
  bool astrometry;
  MuniLayer layer;
};

wxDECLARE_EVENT(EVT_ASTROMETRY,MuniAstrometryEvent);

//#define MuniAstrometryEventHandler(func) (&func)


// -- Photometry

class MuniPhotometryEvent: public wxCommandEvent
{
 public:
 MuniPhotometryEvent(WXTYPE e =wxEVT_NULL, int id =wxID_ANY): wxCommandEvent(e,id),erase(false) {}
  MuniPhotometryEvent(wxCommandEvent& e): wxCommandEvent(e),erase(false) {}
  wxEvent *Clone(void) const { return new MuniPhotometryEvent(*this); }
  bool erase;
  MuniLayer layer;
};

wxDECLARE_EVENT(EVT_PHOTOMETRY,MuniPhotometryEvent);

// -- Thread

// pssing of rendered image in thread

class MuniRenderEvent: public wxThreadEvent
{
 public:
 MuniRenderEvent(WXTYPE commandEventType =wxEVT_NULL, int i =wxID_ANY):
  wxThreadEvent(commandEventType,i),id(0),completed(false),x(0),y(0) {}
 MuniRenderEvent(wxThreadEvent& e):
  wxThreadEvent(e),id(0),completed(false),x(0),y(0) {}
  wxEvent *Clone(void) const { return new MuniRenderEvent(*this); }
  int id;
  bool completed;
  FitsBitmap picture;
  FitsImage image;
  int x,y;
};

wxDECLARE_EVENT(EVT_RENDER,MuniRenderEvent);


// -- fits open

// passing of loaded Fits and Meta

class FitsOpenEvent: public wxThreadEvent
{
 public:
 FitsOpenEvent(WXTYPE commandEventType =wxEVT_NULL, int id =wxID_ANY): wxThreadEvent(commandEventType,id) {}
  FitsOpenEvent(wxThreadEvent& e): wxThreadEvent(e) {}
  wxEvent *Clone(void) const { return new FitsOpenEvent(*this); }
  wxString filename;
  FitsFile fits;
  FitsMeta meta;
  std::vector<FitsTone> tones;
  std::vector<wxImage> icons;
};


wxDECLARE_EVENT(EVT_FITS_OPEN,FitsOpenEvent);

#define MuniFitsOpenEventHandler(func) (&func)


// -- meta open

// passing of loaded Meta

class MetaOpenEvent: public wxThreadEvent
{
public:
  MetaOpenEvent(WXTYPE commandEventType =wxEVT_NULL, int id =wxID_ANY):
    wxThreadEvent(commandEventType,id) {}
  MetaOpenEvent(wxThreadEvent& e): wxThreadEvent(e) {}
  wxEvent *Clone(void) const { return new MetaOpenEvent(*this); }
  FitsMeta meta;
};

wxDECLARE_EVENT(EVT_META_OPEN,MetaOpenEvent);

#define MuniMetaOpenEventHandler(func) (&func)

// -- general draw layer event

class MuniDrawEvent: public wxCommandEvent
{
 public:
 MuniDrawEvent(WXTYPE e =wxEVT_NULL, int id =wxID_ANY): wxCommandEvent(e,id) {}
 MuniDrawEvent(wxCommandEvent& e): wxCommandEvent(e) {}
  wxEvent *Clone(void) const { return new MuniDrawEvent(*this); }
  MuniLayer layer;
};

wxDECLARE_EVENT(EVT_DRAW,MuniDrawEvent);

#endif

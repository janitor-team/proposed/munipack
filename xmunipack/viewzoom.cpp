/*

  xmunipack - MuniView zoom controler

  Copyright © 2019 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "xmunipack.h"
#include <wx/choice.h>
#include <wx/wx.h>

// 8 items, 1:1 is at position 2
const int MuniViewZoom::nratios  = 8;
const int MuniViewZoom::oratio  = 2;
const int MuniViewZoom::ratios[] = {-4,-2,1,2,3,4,6,8};

MuniViewZoom::MuniViewZoom(wxWindow *w, wxWindowID id, int initzoom):
  wxChoice(), current(oratio), bestfit(oratio)
{
  /*
  zoomlist.Add("1:8");
  zoomlist.Add("1:6");
  zoomlist.Add("1:4");
  zoomlist.Add("1:3");
  zoomlist.Add("1:2");
  zoomlist.Add("1:1");
  zoomlist.Add("2:1");
  zoomlist.Add("4:1");
  */

  // setup items in format, and order, above
  for(int n = nratios-1; n >= 0; n--)
    zoomlist.Add(Label(ratios[n]));

  wxChoice::Create(w,id,wxDefaultPosition,wxDefaultSize,zoomlist);

  Bind(wxEVT_CHOICE,&MuniViewZoom::OnChoiceSelect,this);

  for(int i = 0; i < nratios; i++)
    if( ratios[i] == initzoom ) {
      current = i;
      SelectItem(ratios[i]);
      break;
    }
}

void MuniViewZoom::OnChoiceSelect(wxCommandEvent& event)
{
  for(int i = 0; i < nratios; i++)
    if( event.GetString() == zoomlist[i]) {
      current = nratios - i - 1;
      break;
    }
  event.Skip();
}


wxString MuniViewZoom::Label(int n) const
{
  wxString a;
  if( n > 0 )
    a.Printf("1:%d",n);
  else
    a.Printf("%d:1",-n);
  return a;
}

void MuniViewZoom::SelectItem(int n)
{
  wxString label = Label(n);
  for(unsigned int i = 0; i < GetCount(); i++)
    if( label == GetString(i) ) {
      SetSelection(i);
      break;
    }
}

void MuniViewZoom::SetBestFit(int wbox, int hbox, int width, int height)
{
  bestfit = 1;
  for(int l = oratio; l < nratios; l++) {
    int w = width  / ratios[l];
    int h = height / ratios[l];
    if( w < wbox && h < hbox ) {
      bestfit = l;
      break;
    }
  }
}


bool MuniViewZoom::IsNormalSize() const
{
  return current == oratio;
}

bool MuniViewZoom::IsBestFitSize() const
{
  return current == bestfit;
}

bool MuniViewZoom::IsIncable() const
{
  return current > 0;
}

bool MuniViewZoom::IsOutable() const
{
  return current < nratios - 1;
}

int MuniViewZoom::GetValue() const
{
  return ratios[current];
}

void MuniViewZoom::SelectNormalSize()
{
  current = oratio;
  SelectItem(ratios[oratio]);
}

void MuniViewZoom::SelectBestFitSize()
{
  current = bestfit;
  SelectItem(ratios[bestfit]);
}

void MuniViewZoom::SelectIn()
{
  if( current > 0 ) {
    current--;
    SelectItem(ratios[current]);
  }
}

void MuniViewZoom::SelectOut()
{
  if( current < nratios-1 ) {
    current++;
    SelectItem(ratios[current]);
  }
}

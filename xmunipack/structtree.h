/*

  xmunipack - FITS file popup box

  Copyright © 2018 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef _FITS_H_
#include "fits.h"
#endif

#include <wx/wx.h>
#include <wx/treectrl.h>
#include <wx/combo.h>


#ifdef __WXDEBUG__
#include <wx/debug.h>
#endif

class MuniStructtree: public wxTreeCtrl, public wxComboPopup
{

  wxTreeItemId        m_value; // current item index
  wxTreeItemId        m_itemHere; // hot item in popup

  virtual void Init();
  virtual bool Create(wxWindow*);
  virtual wxWindow *GetControl() { return this; }
  virtual wxSize GetAdjustedSize(int,int,int);
  virtual wxSize GetBestSize() const;
  // Needed by SetStringValue
  wxTreeItemId FindItemByText(wxTreeItemId,const wxString& );
  virtual void SetStringValue(const wxString&);
  virtual wxString GetStringValue() const;
  void OnMouseMove(wxMouseEvent&);
  void OnMouseClick(wxMouseEvent&);
  void PaintComboControl(wxDC&, const wxRect&);

 public:

  void SetMeta(const FitsMeta&);
  void SetHdu(int);
};

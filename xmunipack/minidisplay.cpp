/*

  xmunipack - tune panel mini display

  Copyright © 2018 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "tune.h"
#include <wx/wx.h>
#include <wx/dcbuffer.h>
#include <wx/dcmemory.h>


MuniMiniDisplay::MuniMiniDisplay(wxWindow *win, int w, int h):
  wxWindow(win,wxID_ANY), width(w), height(h)
{
  SetBackgroundStyle(wxBG_STYLE_PAINT);
  Bind(wxEVT_PAINT,&MuniMiniDisplay::OnPaint,this);
}

void MuniMiniDisplay::OnPaint(wxPaintEvent& event)
{
  if( ! bitmap.IsOk() ) return;

  wxAutoBufferedPaintDC dc(this);

  wxSize size = GetClientSize();
  int xoff = wxMax((size.GetWidth() - bitmap.GetWidth()) / 2,0);

  // draw image bitmap
  dc.DrawBitmap(bitmap,xoff,0,false);

  // draw decorations
  dc.SetBrush(*wxTRANSPARENT_BRUSH);
  dc.SetPen(*wxBLACK_PEN);
  dc.DrawRectangle(xoff,0,bitmap.GetWidth(),bitmap.GetHeight());
  dc.SetPen(*wxWHITE_PEN);
  dc.DrawRectangle(xoff+1,1,bitmap.GetWidth()-2,bitmap.GetHeight()-2);
}

wxSize MuniMiniDisplay::DoGetBestSize() const
{
  return wxSize(width,height);
}

void MuniMiniDisplay::SetImage(const wxImage& i)
{
  bitmap = wxBitmap(i);
  Refresh();
}

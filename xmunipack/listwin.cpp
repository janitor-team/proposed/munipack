/*

  xmunipack - listwin


  Copyright © 2010 - 2012 F.Hroch (hroch@physics.muni.cz)

  This file is part of Munipack.

  Munipack is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Munipack is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Munipack.  If not, see <http://www.gnu.org/licenses/>.


*/

#include "xmunipack.h"
#include <wx/wx.h>
#include <vector>

using namespace std;


MuniListWindow::MuniListWindow(wxWindow *w, wxWindowID id, long style, MuniConfig *c):
  wxWindow(w,id),config(c),list(0)
{
  if( style & wxLC_ICON )
    list = new MuniListIcon(this,c);
  else if( style & wxLC_REPORT )
    list = new MuniListList(this,c);
  else
    wxFAIL_MSG("--------- Unsupported list type.");

  wxASSERT(list);

  topsizer = new wxBoxSizer(wxVERTICAL);
  topsizer->Add(list,wxSizerFlags(1).Expand());
  SetSizerAndFit(topsizer);

  //  Bind(EVT_FITS_OPEN,&MuniListCtrl::OnMetaOpen,this);
  //  Bind(EVT_META_OPEN,&MuniListCtrl::OnMetaLoad,this);
  //  Bind(wxEVT_END_PROCESS,&MuniBrowser::OnProcess,this);

  /*
  Bind(EVT_FITS_OPEN,&MuniListWindow::OnMetaOpen,this);
  Bind(EVT_META_OPEN,&MuniListWindow::OnMetaLoad,this);
  Bind(wxEVT_END_PROCESS,&MuniListWindow::OnProcess,this);
  */
}


void MuniListWindow::SetStyle(long style)
{

  MuniListCtrl *l = list;

  if( style & wxLC_ICON )
    list = new MuniListIcon(this,config);
  else if( style & wxLC_REPORT )
    list = new MuniListList(this,config);
  else
    wxFAIL_MSG("--------- Unsupported list type.");

  list->SetMeta(l->GetAllMeta());
  topsizer->Replace(l,list);

  // select already selected items ?

  l->Destroy();

  Layout();
}


void MuniListWindow::AddWindow(wxWindow* w)
{
  topsizer->Insert(0,w,wxSizerFlags().Expand());
  Layout();
}

void MuniListWindow::OnProcess(wxProcessEvent& event)
{
  wxQueueEvent(list,event.Clone());
}

void MuniListWindow::OnMetaOpen(FitsOpenEvent& event)
{
  wxQueueEvent(list,event.Clone());
}

void MuniListWindow::OnMetaLoad(MetaOpenEvent& event)
{
  wxQueueEvent(list,event.Clone());
}

void MuniListWindow::AddMeta(const FitsMeta& m)
{
  list->AddMeta(m);
  topsizer->FitInside(list);
}

void MuniListWindow::AddMeta(const std::vector<FitsMeta>& l)
{
  list->AddMeta(l);
}

void MuniListWindow::AddFits(const wxArrayString& f)
{
  list->AddFits(f);
}

void MuniListWindow::SetMeta(const std::vector<FitsMeta>& l)
{
  list->SetMeta(l);
}

void MuniListWindow::SelectAll()
{
  list->SelectAll();
}

void MuniListWindow::DeSelectAll()
{
  list->DeSelectAll();
}

std::vector<FitsMeta> MuniListWindow::GetAllMeta() const
{
  return list->GetAllMeta();
}

std::vector<FitsMeta> MuniListWindow::GetSelectedMeta() const
{
  return list->GetSelectedMeta();
}

std::vector<FitsMeta> MuniListWindow::GetAddedMeta() const
{
  return list->GetAddedMeta();
}

std::vector<FitsMeta> MuniListWindow::GetDeletedMeta() const
{
  return list->GetDeletedMeta();
}

void MuniListWindow::DeleteMeta(const std::vector<FitsMeta>& l)
{
  list->DeleteMeta(l);
}

bool MuniListWindow::DeleteAllMeta()
{
  return list->DeleteAllMeta();
}

void MuniListWindow::Update()
{
  list->Update();
}

std::vector<FitsMeta> MuniListWindow::GetClipboard() const
{
  return list->GetClipboard();
}

void MuniListWindow::SetClipboard(const std::vector<FitsMeta>& l)
{
  list->SetClipboard(l);
}

std::vector<unsigned int> MuniListWindow::GetSelectedIndex() const
{
  return list->GetSelectedIndex();
}

void MuniListWindow::SelectItemLast()
{
  list->SelectItemLast();
}

void MuniListWindow::SelectItem(long i)
{
  list->SelectItem(i);
}

void MuniListWindow::SelectItemRelative(long i)
{
  list->SelectItemRelative(i);
}

int MuniListWindow::GetSelectedItemCount() const
{
  return list->GetSelectedItemCount();
}

int MuniListWindow::GetItemCount() const
{
  return list->GetItemCount();
}

void MuniListWindow::Cut()
{
  return list->Cut();
}

void MuniListWindow::Copy()
{
  return list->Copy();
}

void MuniListWindow::Paste()
{
  return list->Paste();
}

void MuniListWindow::Label(int id)
{
  list->Label(id);
}

void MuniListWindow::Sort(int id)
{
  list->Sort(id);
}

void MuniListWindow::Reverse(bool b)
{
  list->Reverse(b);
}
